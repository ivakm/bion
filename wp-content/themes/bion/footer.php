<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BION
 */

?>

</div><!-- #content -->

<footer>

	<div class="footerContainer">
		<h1>bion<br/>terrarium<br/>center<br/></h1>
		<div>
			<h2>company</h2>
		<?php wp_nav_menu ( array(
			'theme_location' => 'Company',
			'container'      => false,
			'menu_class'     => 'Bion-Company',
			'menu_id'        => '',
			'fallback_cb'    => 'wp_page_menu',
			'depth'          => 0 ) );
		?>
		</div>
		<div>
			<h2>more about bion</h2>
		<?php wp_nav_menu ( array(
			'theme_location' => 'MoreAbout',
			'container'      => false,
			'menu_class'     => 'Bion-MoreAbout',
			'menu_id'        => '',
			'fallback_cb'    => 'wp_page_menu',
			'depth'          => 0 ) );
		?>
		</div>
		<div>
			<h2>follow us</h2>
		<?php wp_nav_menu ( array(
			'theme_location' => 'FallowUs',
			'container'      => false,
			'menu_class'     => 'Bion-FallowUs',
			'menu_id'        => '',
			'fallback_cb'    => 'wp_page_menu',
			'depth'          => 0 ) );
		?>
		</div>
		<ul>
			<li>&copy; BION terrarium center.</li>
			<li>No material and/or photomay be used<br/>
				without reference to www.bion.com.ua
			</li>

			<p>Made with love by </p>
			<img src="" alt="White&red">
		</ul>
	</div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer (); ?>

</body>
</html>
