<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BION
 */

?>
<!doctype html>
<html <?php language_attributes (); ?>>
<head>
	<meta charset="<?php bloginfo ( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://bootswatch.com/cerulean/bootstrap.min.css  ">

	<?php wp_head (); ?>
</head>

<body <?php body_class (); ?>>
<div id="page" class="site">

	<header>

		<div class="HeaderMenu">
			<div class="HeaderLogo"><img
					src="<?php echo wp_get_attachment_image_src ( get_theme_mod ( 'custom_logo' ), 'full' )[0] ?>"
					alt="<?php echo ( get_bloginfo ( 'title' ) ); ?>" width="90" height="55"></div>
			<ul class="TopMenu">
		  <?php wp_nav_menu ( array(
			  'theme_location' => 'TopMenu',
			  'container'      => 'div',
			  'menu_class'     => 'Bion-TopMenu clearfix',
			  'menu_id'        => '',
			  'fallback_cb'    => 'wp_page_menu',
			  'depth'          => 0 ) );
		  ?>
			</ul>
			<a class="top_button" href="#">request price-list</a>
		</div>
		<div class="HeaderInfo">
			<h1>BION Terrarium Center</h1>
			<p>Reptiles import-export and breeding since 1993</p>
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
