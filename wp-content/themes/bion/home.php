<?php
/* Template Name: Home */
get_header (); ?>
<?php

while ( have_posts () ) : the_post ();

	/*
	 * Include the Post-Format-specific template for the content.
	 * If you want to override this in a child theme, then include a file
	 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
	 */

endwhile;
?>

<div id="BionContent">
	<div id="BionCounters">
		<ul>
			<li>
				<div class="CountersCOntainer">
					<h1><?php the_field ( 'hsyyear' ) ?></h1>
					<div>
						<span><?php the_field ( 'hsyheader' ) ?></span><br/>
						<p><?php the_field ( 'hsytext' ) ?></p>
					</div>
				</div>
			</li>
			<li>
				<div class="CountersCOntainer">
					<h1><?php the_field ( 'hspnumber' ) ?></h1>
					<div><span><?php the_field ( 'hspheader' ) ?></span><br/>
						<p><?php the_field ( 'hsptext' ) ?></p>
					</div>

				</div>
			</li>
			<li>
				<div class="CountersCOntainer">
					<h1><?php the_field ( 'hssnumber' ) ?></h1>
					<div><span><?php the_field ( 'hssheader' ) ?></span><br/>
						<p><?php the_field ( 'hsstext' ) ?></p></div>
				</div>
			</li>
			<li>
				<div class="CountersCOntainer">
					<h1><?php the_field ( 'hsspNumber' ) ?></h1>
					<div><span><?php the_field ( 'hsspHeader' ) ?></span><br/>
						<p><?php the_field ( 'hsspText' ) ?></p></div>
				</div>
			</li>
			<li>
				<div class="CountersCOntainer">
					<h1><?php the_field ( 'hscNumber' ) ?></h1>
					<div><span><?php the_field ( 'hscheader' ) ?></span><br/>
						<p><?php the_field ( 'hscText' ) ?></p></div>
				</div>
			</li>
			<li>
				<a href="#">more about bion</a>
			</li>
		</ul>
	</div>
	<div class="clean"></div>
	<div id="Advantages">
		<h1>Advantages</h1>
		<div class="AdvantagesContainer">
		<?php wp_nav_menu ( array(
			'theme_location' => 'Adventage1',
			'container'      => false,
			'menu_class'     => 'Bion-TopMenu clearfix',
			'menu_id'        => '',
			'fallback_cb'    => 'wp_page_menu',
			'depth'          => 0 ) );
		?>
		<?php wp_nav_menu ( array(
			'theme_location' => 'Adventage2',
			'container'      => false,
			'menu_class'     => 'Bion-TopMenu clearfix',
			'menu_id'        => '',
			'fallback_cb'    => 'wp_page_menu',
			'depth'          => 0 ) );
		?>
		</div>
		<div class="clean"></div>
		<div class="clean"></div>
		<a href="#">View Stock-list</a>
	</div>
	<div class="clean"></div>
	<div id="BionDiscounts">
		<h1>Discounts</h1>
		<p>All BION’s wholesale customers are entitled to special prices and discounts <br/>(depending
			on quantity) as well as privileged shipping cost, namely</p>
		<ul>
			<li>
				<span><?php the_field ( 'discountheader1' ) ?></span><br/>
		  <?php the_field ( 'discounttext1' ) ?>
			</li>
			<li>
				<span><?php the_field ( 'discountheader1' ) ?></span><br/>
		  <?php the_field ( 'discounttext1' ) ?>
			</li>
			<li>
				<span><?php the_field ( 'discountheader3' ) ?></span><br/>
		  <?php the_field ( 'discounttext3' ) ?>
			</li>
		</ul>
		<a href="#">Learn more</a>
	</div>
	<div class="clean"></div>
	<div id="News">
		<div class="NewsContainer">
			<ul>
				<li>
					<h1>news</h1>
				</li>
				<li>
					<a href="#">view all</a>
				</li>
			</ul>
			<div class="ContainerNews">

		  <?php

		  $args = array(
			  'numberposts'      => 4,
			  'category'         => 0,
			  'orderby'          => 'date',
			  'order'            => 'DESC',
			  'include'          => array(),
			  'exclude'          => array(),
			  'meta_key'         => '',
			  'meta_value'       => '',
			  'post_type'        => 'post',
			  'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
		  );

		  $posts = get_posts ( $args );

		  foreach ( $posts as $post ) {
			  setup_postdata ( $post );
			  ?>
						<div class="NewsRow">
							<div class="NewsRowLeft"><p><?php echo get_the_date ( 'j' ) ?></p>
								<span><?php echo get_the_date ( 'M' ) ?></span>
							</div>
							<div class="NewsRowRight"><a href="<?php echo get_the_permalink () ?>"><?php echo get_the_title () ?></a>
							</div>
						</div>

			  <?php
		  }
		  wp_reset_postdata (); // сброс

		  ?>
			</div>
		</div>
	</div>
</div>
</div>
<div class="clean"></div>
<div id="BIONform">
	<div class="FormContainer">
		<h1>in case of any questions dpor use a line</h1>

		<?php echo do_shortcode('[contact-form-7 id="4" title="HomePageContactForm"]') ?>
	</div>
</div>
<div class="clean">
</div>
</div>


<?php
get_footer ();
?>
